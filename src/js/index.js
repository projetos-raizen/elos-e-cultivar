
$(document).ready(function () {
    function hideLoader() {
        $('body').removeClass('activeLoader');
        $('#loader').fadeOut();
    }

    setTimeout(hideLoader, 3000);

    $(window).scroll(function () {
        if ($(window).width() <= 1024) {
            if ($(this).scrollTop() >= 30) {
                $('body').addClass('menuActive');
            } else {
                $('body').removeClass('menuActive');
            }
        } else {
            if ($(this).scrollTop() >= 108) {
                $('body').addClass('menuActive');
            } else {
                $('body').removeClass('menuActive');
            }
        }
    });

    $('body .hamb-background').click(function (e) {
        $('body').toggleClass('modalMobileActive');
    });


    $('body header nav a, body footer nav a').click(function (e) {
        e.preventDefault();

        var target = $(this.hash);
        $('html, .ubirata').animate(
            {
                scrollTop: target.offset().top - 180,
            },
            800
        );
    });

    $('.arrow-down a, .modalMenuMobile nav a, .register-logo a, .register-and-know-more a').click(function (e) {
        e.preventDefault();
        $('body').removeClass('modalMobileActive');

        var target = $(this.hash);
        $('html, .ubirata').animate(
            {
                scrollTop: target.offset().top - 100,
            },
            800
        );
    });

    // MENU-ACTIVE

   // Variável para rastrear a classe ativa atual
    var activeClass = 'active';

    // Função para atualizar a classe ativa nas hashes das âncoras
    function updateActiveHashClass() {
        var visibleSection = null;

        // Itera sobre as âncoras do menu
        $('header nav a').each(function () {
            var targetId = $(this).attr('href').substring(1);
            var target = $('#' + targetId);

            // Verifica se a seção está visível
            if (isSectionInViewport(target)) {
                visibleSection = targetId;
            }
        });

        // Adiciona classe ativa às âncoras no menu
        $('header nav a').removeClass(activeClass); // Remove a classe ativa de todas as âncoras
        $('header nav a[href="#' + visibleSection + '"]').addClass(activeClass); // Adiciona a classe ativa à âncora visível
    }

    // Função para verificar se uma seção está visível na tela
    function isSectionInViewport(element) {
        if (element.length) {
            // Distância do topo definida pela animação
            var offset = 200;

            // Calcula a posição do topo e do final do elemento
            var elementTop = element.offset().top - offset;
            var elementBottom = elementTop + element.outerHeight();

            // Calcula a posição do topo e do final da tela
            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();

            // Verifica se o topo do elemento está a 100px do topo da tela
            var isTopVisible = elementTop <= viewportTop && elementBottom >= viewportTop;

            // Retorna true se o topo estiver visível e o elemento estiver totalmente na tela
            return isTopVisible;
        }
        return false;
    }

    // Aguarda o carregamento completo da página
    $(window).on('load', function () {
        updateActiveHashClass();
    });

    // Atualiza a classe ativa ao rolar a página
    $(window).scroll(function () {
        updateActiveHashClass();
    });

    const registerForm = $('.register-form');
    const button = $('.register-and-know-more a:nth-child(1)');

    function handleButtonVisibility() {
      const scrollTop = $(window).scrollTop();
      const formTop = registerForm.offset().top;
      const windowHeight = $(window).height();

      if (scrollTop + windowHeight >= formTop) {
        button.addClass('fade-out')
      } else {
        button.removeClass('fade-out')
      }
    }

    // Verifica a visibilidade inicial do formulário e oculta o botão se necessário
    handleButtonVisibility();

    $(window).on('scroll resize', function () {
      handleButtonVisibility();
    });

    $(window).on('load', function () {
        // Adiciona a classe ativa ao primeiro item do menu
        $('header nav a:first').addClass(activeClass);
        updateActiveHashClass();
    });
});